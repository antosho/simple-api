<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ItemReviewCommentRepository")
 */
class ItemReviewComment extends EntityBase
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ItemReview", inversedBy="itemReviewComments")
     */
    private $review;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ItemReviewComment", inversedBy="itemReviewComments")
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ItemReviewComment", mappedBy="comment")
     */
    private $itemReviewComments;

    public function __construct()
    {
        $this->itemReviewComments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getReview(): ?ItemReview
    {
        return $this->review;
    }

    public function setReview(?ItemReview $review): self
    {
        $this->review = $review;

        return $this;
    }

    public function getComment(): ?self
    {
        return $this->comment;
    }

    public function setComment(?self $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getItemReviewComments(): Collection
    {
        return $this->itemReviewComments;
    }

    public function addItemReviewComment(self $itemReviewComment): self
    {
        if (!$this->itemReviewComments->contains($itemReviewComment)) {
            $this->itemReviewComments[] = $itemReviewComment;
            $itemReviewComment->setComment($this);
        }

        return $this;
    }

    public function removeItemReviewComment(self $itemReviewComment): self
    {
        if ($this->itemReviewComments->contains($itemReviewComment)) {
            $this->itemReviewComments->removeElement($itemReviewComment);
            // set the owning side to null (unless already changed)
            if ($itemReviewComment->getComment() === $this) {
                $itemReviewComment->setComment(null);
            }
        }

        return $this;
    }
}
