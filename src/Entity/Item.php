<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 */
class Item extends EntityBase
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="float")
     */
    private $rating;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ItemReview", mappedBy="item", orphanRemoval=true)
     */
    private $review;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ItemCategory", inversedBy="items")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ItemImage", inversedBy="items")
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ItemTag", inversedBy="items")
     */
    private $tags;

    public function __construct()
    {
        $this->review = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRating(): ?float
    {
        return $this->rating;
    }

    public function setRating(float $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return Collection|ItemReview[]
     */
    public function getReview(): Collection
    {
        return $this->review;
    }

    public function addReview(ItemReview $review): self
    {
        if (!$this->review->contains($review)) {
            $this->review[] = $review;
            $review->setItem($this);
        }

        return $this;
    }

    public function removeReview(ItemReview $review): self
    {
        if ($this->review->contains($review)) {
            $this->review->removeElement($review);
            // set the owning side to null (unless already changed)
            if ($review->getItem() === $this) {
                $review->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemCategory[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(ItemCategory $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(ItemCategory $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    /**
     * @return Collection|ItemImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(ItemImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }

        return $this;
    }

    public function removeImage(ItemImage $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }

        return $this;
    }

    /**
     * @return Collection|ItemTag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(ItemTag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(ItemTag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }
}
