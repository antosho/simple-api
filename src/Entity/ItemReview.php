<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ItemReviewRepository")
 */
class ItemReview extends EntityBase
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="review")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ItemReviewComment", mappedBy="review")
     */
    private $itemReviewComments;

    public function __construct()
    {
        $this->itemReviewComments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    /**
     * @return Collection|ItemReviewComment[]
     */
    public function getItemReviewComments(): Collection
    {
        return $this->itemReviewComments;
    }

    public function addItemReviewComment(ItemReviewComment $itemReviewComment): self
    {
        if (!$this->itemReviewComments->contains($itemReviewComment)) {
            $this->itemReviewComments[] = $itemReviewComment;
            $itemReviewComment->setReview($this);
        }

        return $this;
    }

    public function removeItemReviewComment(ItemReviewComment $itemReviewComment): self
    {
        if ($this->itemReviewComments->contains($itemReviewComment)) {
            $this->itemReviewComments->removeElement($itemReviewComment);
            // set the owning side to null (unless already changed)
            if ($itemReviewComment->getReview() === $this) {
                $itemReviewComment->setReview(null);
            }
        }

        return $this;
    }
}
