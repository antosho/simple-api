<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * EntityBase Interface
 *
 * @author Takasho - Anton Seemesenko <anton_semesenko@takasho.agency>
 */
interface EntityBaseInterface
{
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void;

    /**
     * Get createdAt
     *
     * @return null|DateTime
     */
    public function getCreatedAt();

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     * @return self
     */
    public function setCreatedAt(DateTime $createdAt);

    /**
     * Get updatedAt
     *
     * @return self
     */
    public function getUpdatedAt();

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     * @return self
     */
    public function setUpdatedAt(DateTime $updatedAt);
}