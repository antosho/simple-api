<?php

namespace App\Repository;

use App\Entity\ItemCommentComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ItemCommentComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemCommentComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemCommentComment[]    findAll()
 * @method ItemCommentComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemCommentCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemCommentComment::class);
    }

    // /**
    //  * @return ItemCommentComment[] Returns an array of ItemCommentComment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ItemCommentComment
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
