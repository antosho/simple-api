<?php

namespace App\Repository;

use App\Entity\ItemReview;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ItemReview|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemReview|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemReview[]    findAll()
 * @method ItemReview[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemReview::class);
    }

    // /**
    //  * @return ItemReview[] Returns an array of ItemReview objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ItemReview
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
